//# = = = =
//= # = = =
//= = # = =
//= = = # =
//= = = = #
import java.io.*;
class Demo{
	public static void main(String s []) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("rows ");
		int rows=Integer.parseInt(br.readLine());
		System.out.println(rows);
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows;j++){
				if(i==j){
					System.out.print("#"+ " ");
				}
				else{
					System.out.print("="+ " ");
				}
			}
			System.out.println();
		}
	}
}

