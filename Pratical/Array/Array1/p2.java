/*WAP to take size of array from user and also take integer elements from user Print
product of even elements only
input : Enter size : 9
Enter array elements : 1 2 3 2 5 10 55 77 99
output : 40
// 2 * 2 * 10 */
import java.io.*;
class ProductOfEven{
        public static void main(String s[])throws IOException{
                BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter size n:");
                int n=Integer.parseInt(br.readLine());
                int arr[]=new int[n];
                System.out.println("Enter no of element");
                int mul=1;

                for(int i=0;i<arr.length;i++){

                        arr[i]=Integer.parseInt(br.readLine());
                }
                         for(int i=0;i<arr.length;i++){
                                 if(arr[i]%2==0)
                                   mul=mul*arr[i];
                         }
                                 System.out.println("product of even no :"+ mul);
                         }
}

