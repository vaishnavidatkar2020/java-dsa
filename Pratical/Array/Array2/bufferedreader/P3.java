/*WAP to find the number of even and odd integers in a given array of integers
 * Input: 1 2 5 4 6 7 8
 * Output:
 * Number of Even Elements: 4
 * Number of Odd Elements : 3
 */
 import java.io.*; 
 class Count { 
	 public static void main(String[] args) throws IOException{ 
		 BufferedReader br = new BufferedReader (new
		          InputStreamReader(System.in));    
          		 System.out.println("Enter size  array");               
				 int size = Integer.parseInt(br.readLine());  
			 int arr[] = new int[size];
			 System.out.println("Enter elements");   
			 for(int i=0; i<arr.length; i++) {   
				 arr[i] = Integer.parseInt(br.readLine());    
			 }                  
			 int counteven=0;
			int  countodd=0;		
			 for(int i=0; i<arr.length; i++) {   
				 if(arr[i]%2==0) {
				counteven++;
				}
				else{
				countodd++;				
				 }
			 }		
				System.out.println( "even count of array =" + counteven); 
				  System.out.println("odd count of array = " + countodd);   			   
	 }
 }

