/*Write a Java program to find the sum of even and odd numbers in an array.
 * Display the sum value.
 * Input: 11 12 13 14 15
 * Output
 * Odd numbers sum = 39
 * Even numbers sum = 26
 */
import java.io.*;
class SumOfEvenOdd {
	public static void main(String[] args) throws IOException{
		BufferedReader br = new BufferedReader (new
				InputStreamReader(System.in));     
		System.out.println("Enter size  array");    
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];     
		System.out.println("Enter elements");     
		for(int i=0; i<arr.length; i++) {                  
			arr[i] = Integer.parseInt(br.readLine());                                          
		}     
		int Evensum=0;
		int  Oddsum=0;            
		for(int i=0; i<arr.length; i++) { 


				 if(arr[i]%2==0) { 
					 Evensum=Evensum+arr[i]; 
				 }                                
				 else{      
					 Oddsum=Oddsum+arr[i];   
				 }  
		}	 
				 System.out.println( "even sum of array =" + Evensum);    
				 System.out.println( "odd sum of array =" + Oddsum);			
		}
	}

