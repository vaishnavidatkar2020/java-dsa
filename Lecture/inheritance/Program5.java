class Parent{

	static void fun(){
		System.out.println("IN parent");
	
	}

}
class Child extends Parent{

	static void fun(){
		System.out.println("in child");
	
	}



}

class Client{

	public static void main(String []s){
	
		Parent obj1=new Parent();
		Child obj2=new Child();
		Parent obj3=new Child();
		obj1.fun();
		obj2.fun();
		obj3.fun();
	}

}


