class Demo{
	int x=10;
	int y=20;
	void access(){
		System.out.println(x);
		System.out.println(y);
	}
}
class Child extends Demo{
	int x=100;
	int y=200;
	void access(){
		super.access();
		System.out.println(this.x);
		System.out.println(this.y);
	}
}
class Client{
	public static void main(String [] s){
		Child obj=new Child();
		obj.access();
	}
}
