class Parent{
	int x=10;
	static int y=20;
	Parent(){
		System.out.println("Parent");
	}
}
class Child extends Parent{
	int x =100;
	static int y=200;
	Child(){
		System.out.println("child");
	}
	void access(){ //sop(this);
		       //sop(super)
	System.out.println(super.x);
	        System.out.println(super.y);
		        System.out.println(x);//also (this.x) hidden parameter

			        System.out.println(y);

	}
}
class Client{
	public static void main(String []s){
		Child obj=new Child();
		obj.access();//()hidden parameter
	}
}
