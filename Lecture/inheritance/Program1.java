class Parent{
	Parent(){
		System.out.println("in parent constraction");
	}
	void Parentproperty(){
		System.out.println("flat,etc");
	}
}
class Child extends Parent{
	Child(){
		System.out.println(this);
		 
		System.out.println("in child constractor");
	}
}
class Client{
	public static void main(String []s){
		Child obj2=new Child();
		obj2.Parentproperty();

		System.out.println(obj2);
		System.out.println("---------------------------------");

		  Parent obj3=new Child();
		   System.out.println(obj3);
		   System.out.println("---------------------------------");
		  Parent obj4=new Parent();
		   System.out.println(obj4);
	}
}
