import java.util.*;

class Hospital implements Comparable{
        String patientname=null;
	char bloodgroup= '\0';

        int age=0;
        Hospital(String patientname,char bloodgroup,int age){
                this.patientname=patientname;
                this.age=age;
		this.bloodgroup=bloodgroup;
        }
        public int compareTo(Object obj){
                return (bloodgroup.compareTo(((Hospital)obj).bloodgroup));
        }
public String toString(){
        return bloodgroup;
}
}
class BloodBank{
        public static void main(String []s){
                TreeSet ts=new TreeSet();
                ts.add(new Hospital("Sakshi",'A',21));
                ts.add(new Hospital("prachi",'B',20));
                ts.add(new Hospital("komal",'O',20));
                ts.add(new Hospital("anuja",'B',22));
                System.out.println(ts);
        }
}
