import java.util.*;
class MyClass implements Comparable{
	String str=null;
	MyClass(String str){
		this.str=str;
	}
	public int  compareTo(Object obj){
		return (obj.str).compareTo(str);

	}
	public String ToString(){
		return str;
        }
}
class TreeSetDemo{
        public static void main (String []s){
                TreeSet hs=new TreeSet();
                hs.add(new  MyClass("virat"));
                hs.add(new  MyClass("msd"));
                hs.add(new  MyClass("rohit"));
                hs.add(new  MyClass("msd"));
                System.out.println(hs);
        }
}
