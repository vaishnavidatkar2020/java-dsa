import java.util.*;

class Movies implements Comparable{
	String moviename=null;
	float totcoll=0.0f;
	Movies(String moviename,float totcoll){
		this.moviename=moviename;
		this.totcoll=totcoll;
	}
	public int compareTo(Object obj){
		return (moviename.compareTo(((Movies)obj).moviename));
	}
public String toString(){
	return moviename;
}
}
class TreeSetDemo{
	public static void main(String []s){
		TreeSet ts=new TreeSet();
		ts.add(new Movies("Gadar2",150000.00f));
		ts.add(new Movies("ved",250000.00f));
		ts.add(new Movies("housefull2",100000.00f));
		ts.add(new Movies("omj2",120000.00f));
		System.out.println(ts);
	}
}
