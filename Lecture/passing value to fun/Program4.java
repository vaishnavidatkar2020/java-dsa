import java.util.*;
class Demo{
	public static void main(String s[]){
		Demo obj=new Demo();
		obj.fun(10);
		obj.fun(10.5f);
		obj.fun('A');
		 //obj.fun(10.5); incompalible error possible lossy conversion double can not convert into float
		//obj.fun(true);incompatible error boolon can not be converted to float
	}
	void fun(float x){
		System.out.println("in fun");
		System.out.println(x);
	}
}
