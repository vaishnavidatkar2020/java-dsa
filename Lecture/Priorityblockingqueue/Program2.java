import java.util.concurrent.*;

class Producer implements Runnable{
	BlockingQueue bq;
	Producer(BlockingQueue bq){
		this.bq=bq;
	}
	public void run(){
		for(int i=1;i<=10;i++){
			try{
				bq.put(i);
				System.out.println("producer" + i);
			}
			catch(InterruptedException ie){
			}
			try{
				Thread.sleep(1000);
				}
				catch(InterruptedException ie){
				}
			}
		}
	}
class Consumer implements Runnable{
	BlockingQueue bq;
	Consumer(BlockingQueue bq){
                this.bq=bq;
        }
        public void run(){
                for(int i=1;i<=10;i++){
                        try{
                                bq.take();
                                System.out.println("consumer" + i);
                        }
                        catch(InterruptedException ie){
                        }
                        try{
                                Thread.sleep(7000);
                                }
                                catch(InterruptedException ie){
                                }
                        }
                }
        }
class ProducerConsumer{
	public static void main(String []s){
		BlockingQueue bq=new ArrayBlockingQueue(3);
		Producer produce=new Producer(bq);
		Consumer consume=new Consumer(bq);
		Thread  ProducerThread=new Thread(produce);
		Thread  ConsumerThread=new Thread(consume);
		ProducerThread.start();
		ConsumerThread.start();
	}
}

