class BirdDirection {
    public static void main(String[] args) {
        // Initial direction of the bird
        String currentDirection = "North";
        
        // Array to store the sequence of turns
        String[] turns = {"Right", "Left", "Left"};
        
        // Array representing the compass directions in clockwise order
        String[] directions = {"North", "East", "South", "West"};
        
        // Index representing the current direction on the compass
        int currentIndex = 0; // North is at index 0
        
        for (String turn : turns) {
            if (turn.equals("Right")) {
                currentIndex = (currentIndex + 1) % 4; // Move clockwise
            } else if (turn.equals("Left")) {
                currentIndex = (currentIndex - 1 + 4) % 4; // Move counterclockwise
            }
        }
        
        // Print the final direction
        System.out.println("The final direction of the bird is: " + directions[currentIndex]);
    }
}

