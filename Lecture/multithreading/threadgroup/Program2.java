class MyThread extends Thread{
        MyThread(ThreadGroup tg,String str){
                super(tg,str);
        }
        public void run(){
                System.out.println(getName());
                 System.out.println(Thread.currentThread());
        }
}
        class ThreadGroupDemo{
                public static void main(String [] s){

                        ThreadGroup pthreadGP=new ThreadGroup("C2W");
			MyThread obj1=new MyThread(pthreadGP,"c");
			 MyThread obj2=new MyThread(pthreadGP,"c++");
			  MyThread obj3=new MyThread(pthreadGP,"javads");
                        obj1.start();
			 obj2.start();
			  obj3.start();
			   ThreadGroup cthreadGP=new ThreadGroup("other");
			    MyThread obj4=new MyThread(cthreadGP,"c");
                         MyThread obj5=new MyThread(cthreadGP,"c++");
                          MyThread obj6=new MyThread(cthreadGP,"javads");
			   obj4.start();
			   obj5.start();
			   obj6.start();
                }
        }
