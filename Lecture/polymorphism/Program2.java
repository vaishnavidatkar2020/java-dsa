class Parent{
	Parent(){
		System.out.println("parent const");
	}
	void property(){
		System.out.println("home");
	}
	void marry(){
		System.out.println("Deepika padukon");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("child const");
	}
	void marry(){
		System.out.println("alia");
	}
}
class Client{
	public static void main(String []s){
		Child obj=new Child();
		obj.property();
		obj.marry();
	}
}
