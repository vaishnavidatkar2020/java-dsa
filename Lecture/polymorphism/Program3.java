class Parent{
	Parent(){
		System.out.println("Parent const");
	}
	void fun(){
		System.out.println("in fun");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("child const");
	}
	void gun(){
		System.out.println("in gun");
	}
}
class Client{
public static void main (String []s){
	Child obj1=new Child();
	obj1.fun();
	obj1.gun();
	Parent obj2=new Parent();
	obj2.fun();
//	obj2.gun(); gun is not present in parent class
}
}
		

