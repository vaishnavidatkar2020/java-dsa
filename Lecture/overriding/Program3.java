class Parent{
	Parent(){
		System.out.println("parent const");
	}
	void fun(){
		System.out.println("in parent fun");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("child");
	}
}

