class Parent{
	Parent(){
		System.out.println("parent cont");
	}
	void fun(){
		System.out.println("in parant fun");
	}
}
class Child extends Parent{
	Child(){
		System.out.println("child con");
	}
	void fun(){
		System.out.println("child fun");
	}
}
class Client{
	public static void main(String []s){
		Parent obj1=new Child();
		obj1.fun();
	}
}
