class Parent{
	Parent(){
		System.out.println("parent const");
	}
	void fun(int x ){
		System.out.println("in parent ");
		}
	}
class Child extends Parent {  
	Child(){
		System.out.println("in child const");
	}
		void fun(){
				System.out.println("hey child");
		}		
	}

class Client{
	public static void main(String []s){
		int x=3;
		Parent obj1=new Child();
		obj1.fun(x);

	}
}
