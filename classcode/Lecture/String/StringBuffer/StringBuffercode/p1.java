class StringBufferDemo{
	public static void main(String s[]){
	       StringBuffer sb=new StringBuffer("Good");
	       System.out.println(System.identityHashCode(sb));
               //String str="shashi";	Error:incompatibleType (it will declear in string only not in stringbuffer)
	       sb.append("Vibes");
		System.out.println(sb);
 		System.out.println(System.identityHashCode(sb));
		System.out.println(sb.capacity());//capacity=16+stringcount;if we append any string beyond capacity then it will create new obj
				}
}	
